# Backend
![backend](backend.png)

## Acceso

![loginBackend](loginBackend.png)

Para acceder al backend del proyecto es necesario acceder al siguiente link:

[http://Servidor.web/proyectoTypo3/typo3/](http://Servidor.web/proyectoTypo3/typo3/)

#### Obtener distribución preconfigurada

![distro](distro.png)

### Secciones

#### Web
Esta sección contiene los elementos básicos para la configuración de todas las paginas, además de la creación de nuevas páginas.

![web](Web.png)


#### File

Esta sección contiene el gestor de archivos internos de typo3, donde se encuentran, por ejemplo, las imágenes de los templates.

![file](File.png)

#### Herramientas de Administrador

En esta sección se listan las extensiones que permiten agregar nuevas funcionalidades a typo3, como por ejemplo el lenguaje del backend y el creador de extensiones de typo3.

![Admin](Admin.png)

#### Sistema
Esta sección corresponde a la configuración de typo3. Donde es posible configurar los accesos, los usuarios de backend, las opciones de instalación, entre otras cosas.

![Systema](Systema.png)

#### Barra Superior

![BarraSuperior](BarraSuperior.png)

- Estrella: Permite añadir secciones de favoritos al explorador.
- Rayo: Elimina los caches de typo3, pueden ser los del frontend, o los generales.
- Documentación: Permite el acceso a la documentación y a los manuales de typo3.
- Información: Entrega información general de donde se instaló typo3.
- Usuario: Permite desloguearse y acceder a la configuración del usuario de backend.
- Lupa: Permite hacer búsqueda de diversos elementos dentro del backend de typo3.
