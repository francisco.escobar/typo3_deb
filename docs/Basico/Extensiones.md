# Extensiones

Para la gestión de extensiones es necesario acceder a la sección de "Extensions".

![Extensiones](Extensiones.png)


En la Barra superior es posible selecionar entre 3 distintas secciones:

## "Installed Extensions"
En esta sección se encuentran todas las extenciones instaladas en el sitio.
Las acciones que se pueden realizar en esta sección son:

1. Activar/Desactivar
2. Descargar codigo fuente de extensiones
3. Eliminar Extensión
4. (Opcional)configuraciones
5. (Opcional)Notificaciónes

## "Get Extensions"
En esta sección es posible:

1. Instalar extensiones del repositorio de typo3
2. Subir e instalar extensiones propias o personalizadas
3. Actualizar extensiones

Para Subir e instalar extensiones es necesario desplegar el formulario para subir Extensiones.

![upload](upload.png)

1. Seleccionar el archivo comprimirdo en formato "ZIP"
2. Marcar la opción se sobre escritura en caso de tener una extensión con el mismo nombre una vez instalada.
3. Presionar el botón de "upload".


## "Get Preconfigured distribution"

Las distribuciones pre-configuradas, consisten en un conjunto de plugins que permiten el uso del CMS de forma rapida con una configuración por defecto.

El paquete de introducción es el más indicado para utilizar typo3 para aprender a utilizar esta plataforma.
