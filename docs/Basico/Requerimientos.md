# Requerimientos Previos

## Web Server
### Apache
#### Ubuntu 18.04

``` bash
sudo apt update
sudo apt install apache2

```

## PHP 7

#### Ubuntu 18.04

``` bash
# php 7 + apache2
sudo apt install php libapache2-mod-php

sudo systemctl restart apache2

```

####  modulos PHP

``` bash
# gd modulo
sudo apt install php7.2-gd

# mysqli modulo
sudo apt install php7.2-mysql

# xml modulo
sudo apt install php7.2-xml

# zip modulo
sudo apt install php7.2-zip

####### typo3-9 #######
# intl modulo
sudo apt install php7.2-intl

```
#### Modificación de Parametros de php7

Es necesario modificar el archivo de configuración de php:
``` bash
sudo vim /etc/php/7.2/apache2/php.ini
```
cambiando los siguientes parametros

``` bash
max_execution_time=30 -> max_execution_time=240
max_input_vars=1000 -> max_input_vars=1500

```

## MySQL

#### Ubuntu 18.04

``` bash
sudo apt update
sudo apt install mysql-server
sudo mysql_secure_installation

```

##### Crear Usuario MySQL

``` mysql
GRANT ALL PRIVILEGES ON *.* TO 'username'@'localhost' IDENTIFIED BY 'password';
FLUSH PRIVILEGES;

```
