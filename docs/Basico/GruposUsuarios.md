# Grupos y Usuarios

![GruposUsuarios](GruposUsuarios.png)

Desde las versiones 8 en adelante, la creación de grupos de usuarios y usuarios de frontend esta previamente configurado, sin embargo,  es posible distintas configuraciones.

## Nueva Configuración

1. Crear Carpeta
2. Asignar nombre a Carpeta
3. Habilitar visualización
4. Configurar Carpeta
    1. Editar Configuración
    2. Ir a la pestaña de comportamiento
    3. configurar para "Website Users"

![config](config.png)

### Grupos
Es necesario crear al menos un grupo para crear correctamente a un usuario.

Para crear un grupo es necesario seguir los siguientes pasos:

1. Acceder a la carpeta de usuarios
2. ver en modo "List"
3. Agregar un elemento de tipo "Website Usergroup" y darle un nombre al nuevo grupo.

![grupo](grupo.png)

* Luego simplemente con el boton "+" es posible crear nuevos grupos

### Usuarios

Para crear usuarios es necesario seguir los siguientes pasos:


1. Acceder a la carpeta de usuarios
2. Ver en modo "List"
3. Agregar un nuevo elementro del tipo "Website User"
    1. Asignar nombre
    2. Asignar contraseña
    3. Asignar Grupo

![user](user.png)

* Luego simplemente con el boton "+" es posible crear nuevos usuarios

## Añadir Formulario login

Agregar el elemento idealmente en una página visible desde el inicio "Congratulations".
![paginas](paginas.png)

Agrega el elemento de formulario de inicio de sesión.
![formulario](formulario.png)

Configurar el Formulario

![form](configuracionForm.png)

Para configurar el formulario de forma correcta es necesario selecionar la configuración de usuarios deseada.

![form](configuraciónForm2.png)

El formulario se verá de la siguiente forma

![form](formulariofront.png)


### Añadir al template (Opcional)
Esta opcion permite agregar el formulario al inicio de todas las paginas del sitio.
Para lograr eso es necesario realizar los siguientes pasos:

![Typo3 - Template](formTemplateT39.png)

1. Agregar el formulario a la configuración del template de typo3

[Template Typo3 v8 - Setup](Typo3Form-8.txt)


[Template Typo3 v9 - Constants](templateTypo3-9.txt)

[Template Typo3 v9 - Setup](Typo3Form-9.txt)

2. Agregar las configuraciones del template según corresponda a la versión de Typo3 utilizada en la dirección

```/ruta/al/proyecto/fileadmin/ ```

[Templates Typo3 v8 - Templates](Templates8.zip)

[Templates Typo3 v9 - Templates](Templates9.zip)


![loginForm](loginForm.png)
