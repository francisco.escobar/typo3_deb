# Instalación

## Requisistos

Para instalar los prerequisitos puede dirigirse al siguiente [link](../Requerimientos).

### typo3 - Versión 9

- web server (Apache, Nginx, IIS…)
- PHP 7.2 or later
- MySQL 5.5+ / MariaDB / Postgres / SQLite support
- Internet Explorer 11 and later

### typo3 - Version 8

- web server (Apache, Nginx, IIS…)
- PHP 7.0 or later
- MySQL 5.5+ / MariaDB / Postgres support
- Internet Explorer 11 and later


## Obtener Typo3
La ultima versión siempre se obtiene desde el link en la web de Typo3.

### typo3 - Versión 9

[v9](https://get.typo3.org/version/9)

``` bash tab="cUrl - wget"
# Using wget
wget --content-disposition https://get.typo3.org/9

# Using cURL (e.g., when wget fails with SSL error)
curl -L -o typo3_src.tgz https://get.typo3.org/9
```

``` bash tab="GIT"
git clone https://github.com/TYPO3/TYPO3.CMS
```

``` bash tab="Composer"
composer create-project typo3/cms-base-distribution my-new-project 9.5

```

``` bash tab="Source Code"

https://get.typo3.org/9/tar.gz
https://get.typo3.org/9/zip
https://get.typo3.org/9/tar.gz.sig
https://get.typo3.org/9/zip.sig

```



### typo3 - Version 8

[v8](https://get.typo3.org/version/8)


``` bash tab="cUrl - wget"
# Using wget
wget --content-disposition https://get.typo3.org/8

# Using cURL (e.g., when wget fails with SSL error)
curl -L -o typo3_src.tgz https://get.typo3.org/8
```

``` bash tab="GIT"
git clone https://github.com/TYPO3/TYPO3.CMS
```

``` bash tab="Composer"
composer create-project typo3/cms-base-distribution my-new-project 8.7

```

``` bash tab="Source Code"

https://get.typo3.org/8/tar.gz
https://get.typo3.org/8/zip
https://get.typo3.org/8/tar.gz.sig
https://get.typo3.org/8/zip.sig

```

### Descomprimir paquete.

``` bash
# descomprimir tar
tar xzf typo3_src-x.x.xx.tar.gz

# descomprimir zip
unzip typo3_src-x.x.xx.zip
```

## Instalacion

### 1. Carpeta Fuente
Crear Carpeta con el codigo fuente y copiar codigo fuente en esa Carpeta

``` bash

cd /var/www/html
mkdir source
cp -r ~/typo3_src-x.x.xx /var/www/html/source

```
### 2. Crear proyecto
Crear carpeta de proyecto con el nombre deseado

``` bash
cd /var/www/html
mkdir proyectoTypo3

chmod -R 777 proyectoTypo3

```

#### Crear Links

``` bash
cd /var/www/html/proyectoTypo3

ln -s /var/www/html/source/typo3_src-x.x.xx typo3_src
ln -s typo3_src/typo3/ typo3
ln -s typo3_src/index.php index.php
```

#### Crear Archio vacio

El archivo vacio sirve de bandera para indicar la nue va instalación.
``` bash
cd /var/www/html/proyectoTypo3

touch FIRST_INSTALL
```

#### Crear Archivo de configuración de  apache para Typo3 - 8

``` bash
cp typo3_src/_.htaccess .htaccess
```

### 3. Instaiación Web

#### Acceder Instalación web

[http://Servidor.web/proyectoTypo3/](http://Servidor.web/proyectoTypo3/)

Si todos los prerequisitos para instalar typo3 estan correctos se visualizará la siguiente imagen.

![web](webInstall.png)

En esta pantalla, hacer clic en "System looks good. Continue!"

#### Configurar MySQL

![paso2](paso2.png)

Los datos de configuración deben ser consultados al administrador de base de datos.

#### Crear Nuva Base de datos

![paso3](paso3.png)

Siempre crear una base de datos nueva con el nombre del proyecto a crear.

#### Crear usuario backend

![paso4](paso4.png)

Crear el usuario y contraseña. Ademas del nombre del sito que se creará.

*** La contraseña no se mostrará nuevamente ***

#### Finalizar Instalación

![paso5](paso5.png)

Marcar la Primera opción


#### Ingresar a backend

![loginBackend](loginBackend.png)

Para acceder al backend del proyecto es necesario acceder al siguiente link:

[http://Servidor.web/proyectoTypo3/typo3/](http://Servidor.web/proyectoTypo3/typo3/)

#### Obtener distribución preconfigurada

![distro](distro.png)
